<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Db\Countrys;
use App\Db\Leagues;
use App\Db\Matchs;
use App\Db\Teams;
use App\Db\Sports;
use App\Db\Users;
use App\Db\Favorits;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getProfile(){
        $sports = Sports::where('show',1)->get();
        $favorits = Favorits::where('user',Auth::user()->id)->get();
        $data = [
            'favorits' => $favorits,
            'sports' => $sports,
        ];
        return view('user.profile')->with($data);
    }
}
