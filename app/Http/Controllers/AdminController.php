<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Db\Countrys;
use App\Db\Leagues;
use App\Db\Matchs;
use App\Db\Teams;
use App\Db\Sports;
use App\Db\Users;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(Request $request){
        if(Auth::user()->access == 0){
            return redirect()->route('home')->with('warning','Na túto akciu nemáte oprávnenie.');
        }
        if(!$request->session()->has('select_sport')){
            $request->session()->forget('select_sport');
            return redirect()->route('admin.select.sport');
        }
        $matches = Matchs::where('sport',$request->session()->get('select_sport'))->get();
        $teams = Teams::where('sport',$request->session()->get('select_sport'))->get();
        $leagues = Leagues::where('sport',$request->session()->get('select_sport'))->get();
        $sports = Sports::get();
        $data = [
            'sports' => $sports,
            'matchs' => $matches,
            'teams' => $teams,
            'leagues' => $leagues,
        ];
        return view('admin.home')->with($data);
    }

    public function getSelectSport(Request $request){
        $request->session()->forget('select_sport');
        $sports = Sports::get();
        $data = [
            'sports' => $sports,
        ];
        return view('admin.pages.select_sport')->with($data);
    }

    public function getAddMatch(Request $request) {
        $teams = Teams::where('sport',$request->session()->get('select_sport'))->get();
        $leagues = Leagues::where('sport',$request->session()->get('select_sport'))->get();
        $sports = Sports::get();
        $data = [
            'sports' => $sports,
            'teams' => $teams,
            'leagues' => $leagues,
        ];
        return view('admin.pages.addMatch')->with($data);
    }

    public function getAddTeam(Request $request) {
        $countrys = Countrys::get();
        $leagues = Leagues::where('sport',$request->session()->get('select_sport'))->get();
        $teams = Teams::where('sport',$request->session()->get('select_sport'))->get();
        $sports = Sports::get();
        $data = [
            'sports' => $sports,
            'countrys' => $countrys,
            'leagues' => $leagues,
            'teams' => $teams,
        ];
        return view('admin.pages.addTeam')->with($data);
    }

    public function getAddLeague() {
        $countrys = Countrys::get();
        $sports = Sports::get();
        $data = [
            'sports' => $sports,
            'countrys' => $countrys,
        ];
        return view('admin.pages.addLeague')->with($data);
    }

    public function getSelectSportShow($id){
        if(Sports::where('id',$id)->update(['show' => 1])){
            return back()->with('message','Šport bol ukázaný.');
        }else {
            return back()->with('warning', 'Šport sa nepodarilo ukázať. ');
        }
    }

    public function getSelectSportHide($id){
        if(Sports::where('id',$id)->update(['show' => 0])){
            return back()->with('message','Šport bol v menu schovaý.');
        }else {
            return back()->with('warning', 'Šport sa nepodarilo schovať. ');
        }
    }

    //POST

    public function postSelectSport(Request $request){
        $request->session()->put('select_sport', $request->input('id'));
        if($request->session()->has('select_sport')){
            return redirect()->route('admin.index')->with('message','Šport bol vytvorený.');
        }else {
            return redirect()->route('admin.select.sport')->with('warning', 'Šport sa nepodarilo vytvoriť. ');
        }
    }

    public function postAddMatch(Request $request){
        if($request->input('score_team_2') == $request->input('score_team_1')){
            $draw = 1;
        }else{
            $draw = 0;
        }

        $insert = Matchs::insert([
            "sport" => $request->session()->get('select_sport'),
            "team_1" => $request->input('team_1'),
            "score_team_1" => $request->input('score_team_1'),
            "score_team_2" => $request->input('score_team_2'),
            "team_2" => $request->input('team_2'),
            "team_home" => $request->input('home'),
            "league" => $request->input('league'),
            "draw" => $draw,
            "datum" => $request->input('datum'),
            "kurz_vyhra" => $request->input('kurz_vyhra'),
            "kurz_remiza" => $request->input('kurz_remiza'),
            "kurz_prehra" => $request->input('kurz_prehra'),
        ]);
        if($insert){
            return back()->with('message','Zápas bol vytvorený.');
        }else {
            return back()->with('warning', 'Zápas sa nepodarilo vytvoriť. ');
        }
    }
    public function postAddTeam(Request $request){
        dd($request);


    }
    public function postAddLeague(Request $request){
        $insert = Leagues::insert([
            "sport" => $request->session()->get('select_sport'),
            "name" => $request->input('name'),
            "country" => $request->input('country'),
        ]);

        if($insert){
            return back()->with('message','Liga bola vytvorená.');
        }else {
            return back()->with('warning', 'Ligu sa nepodarilo vytvoriť. ');
        }
    }
}
