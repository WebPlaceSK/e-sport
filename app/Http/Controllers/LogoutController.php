<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Db\Countrys;
use App\Db\Leagues;
use App\Db\Matchs;
use App\Db\Sports;
use App\Db\Teams;

class LogoutController extends Controller
{

    public function mustLogin(){
        return redirect()->route('home')->with('warning','Pre túto akciu musíte byť prihlásený.');
    }
    public  function index(){
        $sports = Sports::where('show',1)->get();
        $data = [
            'sports' => $sports,
        ];
        return view('home')->with($data);
    }

    public function contact(){
        $sports = Sports::where('show',1)->get();
        $data = [
            'sports' => $sports,
        ];
        return view('pages.contact')->with($data);
    }

    public function getLeagueList($sport){
        $sports = Sports::where('show',1)->get();
        $leaguse = Leagues::where('sport',$sport)->get();
        $countrys = Countrys::get();
        $data = [
            'sports' => $sports,
            'leagues' => $leaguse,
            'countrys' => $countrys,
            'select_sport' => $sport,
        ];
        return view('pages.leagues')->with($data);
    }

    public function getLeagueListDetail($sport, $league){
        $sports = Sports::where('show',1)->get();
        $matchs = Matchs::where([['sport',$sport],['league',$league]])->get();
        $teams = Teams::where([['sport',$sport],['league',$league]])->get();
        $match_data = null;
        foreach($teams as $team){
            $goal_1 = 0;
            $goal_2 = 0;
            $win = 0;
            $draw = 0;
            $lose = 0;
            $forma = array();
            foreach(Matchs::where([['sport',$sport],['team_1',$team->id]])->get() as $match){
                $goal_1 = $goal_1 + $match->score_team_1;
                $goal_2 = $goal_2 + $match->score_team_2;
                if($match->score_team_1 > $match->score_team_2){
                    $win = $win + 1;
                    $form = [
                        'stav' => 2,
                        'match' => $match->id,
                    ];
                }elseif($match->score_team_1 == $match->score_team_2){
                    $draw = $draw + 1;
                    $form = [
                        'stav' => 1,
                        'match' => $match->id,
                    ];
                }else{
                    $lose = $lose + 1;
                    $form = [
                        'stav' => 0,
                        'match' => $match->id,
                    ];
                }
                if(count($forma) < 10){
                    $forma[] = $form;
                }
            }
            foreach(Matchs::where([['sport',$sport],['team_2',$team->id]])->get() as $match){
                $goal_1 = $goal_1 + $match->score_team_2;
                $goal_2 = $goal_2 + $match->score_team_1;
                if($match->score_team_2 > $match->score_team_1){

                    $win = $win + 1;
                    $form = [
                        'stav' => 2,
                        'match' => $match->id,
                    ];
                }elseif($match->score_team_2 == $match->score_team_1){
                    $draw = $draw + 1;
                    $form = [
                        'stav' => 1,
                        'match' => $match->id,
                    ];
                }else{
                    $lose = $lose + 1;
                    $form = [
                        'stav' => 0,
                        'match' => $match->id,
                    ];
                }
                if(count($forma) < 10){
                    $forma[] = $form;
                }
            }
            $body = (($win * 3) + ($draw * 1));
            $match_data[] = [
                'team' => $team->name,
                'matches' => Matchs::where([['sport',$sport],['team_1',$team->id]])->orwhere([['sport',$sport],['team_2',$team->id]])->count(),
                'win' => $win,
                'draw' => $draw,
                'lost' => $lose,
                'gols' => $goal_1.' : '.$goal_2,
                'bod' => $body,
                'forma' => $forma
                ];
        }
        $data = [
            'sports' => $sports,
            'matches' => $matchs,
            'matches_data' => $match_data,
            'select_league' => $league,
            'select_sport' => $sport,
        ];
        return view('pages.leagues_detail')->with($data);
    }
}
