<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Db\Teams;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teams::insert([
            'sport' => '1',
            'name' => 'Manchester City',
            'img' => 'https://www.flashscore.sk/res/image/data/0vgscFU0-fguToQZ6.png',
            'league' => '1',
            'country' => '6',
            'score' => '0',
        ]);
        Teams::insert([
            'sport' => '1',
            'name' => 'Manchester Utd',
            'img' => 'https://www.flashscore.sk/res/image/data/0vgscFU0-fguToQZ6.png',
            'league' => '1',
            'country' => '6',
            'score' => '0',
        ]);
        Teams::insert([
            'sport' => '1',
            'name' => 'Leicester',
            'img' => 'https://www.flashscore.sk/res/image/data/CvL5rhhT-fguToQZ6.png',
            'league' => '1',
            'country' => '6',
            'score' => '0',
        ]);
        Teams::insert([
            'sport' => '1',
            'name' => 'Leeds',
            'img' => 'https://www.flashscore.sk/res/image/data/lvs4WW5k-EJoO9TRD.png',
            'league' => '1',
            'country' => '6',
            'score' => '0',
        ]);
        Teams::insert([
            'sport' => '1',
            'name' => 'Newcastle',
            'img' => 'https://www.flashscore.sk/res/image/data/UXo7VXPq-fguToQZ6.png',
            'league' => '1',
            'country' => '6',
            'score' => '0',
        ]);
        Teams::insert([
            'sport' => '1',
            'name' => 'Sheffield Utd',
            'img' => 'https://www.flashscore.sk/res/image/data/8nXNvlA6-fguToQZ6.png',
            'league' => '1',
            'country' => '6',
            'score' => '0',
        ]);
    }
}
