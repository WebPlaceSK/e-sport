<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Db\Sports;

class SportsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sports::insert([
            'name' => 'Futbal',
            'show' => '1',
        ]);
        Sports::insert([
            'name' => 'Hokej',
            'show' => '1',
        ]);
        Sports::insert([
            'name' => 'Basketbal',
            'show' => '1',
        ]);
    }
}
