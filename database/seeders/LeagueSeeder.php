<?php

namespace Database\Seeders;

use App\Db\Leagues;
use Illuminate\Database\Seeder;

class LeagueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Leagues::insert([
            'sport' => '1',
            'name' => 'Premier League',
            'country' => '6',
        ]);
        Leagues::insert([
            'sport' => '1',
            'name' => 'Fortuna liga',
            'country' => '158',
        ]);
        Leagues::insert([
            'sport' => '1',
            'name' => 'Ligue 1',
            'country' => '48',
        ]);
        Leagues::insert([
            'sport' => '1',
            'name' => 'Ligue 2',
            'country' => '6',
        ]);
        Leagues::insert([
            'sport' => '1',
            'name' => 'Ligue 3',
            'country' => '158',
        ]);
        Leagues::insert([
            'sport' => '1',
            'name' => 'Ligue 4',
            'country' => '48',
        ]);
    }
}
