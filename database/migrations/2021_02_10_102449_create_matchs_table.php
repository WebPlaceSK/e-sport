<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchs', function (Blueprint $table) {
            $table->id();
            $table->string('sport');
            $table->string('team_1');
            $table->string('team_2');
            $table->string('team_home');
            $table->string('score_team_1');
            $table->string('score_team_2');
            $table->string('draw');
            $table->string('league');
            $table->string('datum');
            $table->string('kurz_vyhra')->nullable();
            $table->string('kurz_remiza')->nullable();
            $table->string('kurz_prehra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchs');
    }
}
