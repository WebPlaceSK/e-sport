<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Site Metas -->
    <title>{{ env('APP_TITLE')}}</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Site Icons -->
    <link rel="shortcut icon" href="" type="image/x-icon" />
    <link rel="apple-touch-icon" href="">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('web/css/bootstrap.min.css')}}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{asset('web/style.css')}}">
{{--    <!-- Colors CSS -->--}}
{{--    <link rel="stylesheet" href="{{asset('web/css/colors.css')}}">--}}
{{--    <!-- ALL VERSION CSS -->--}}
{{--    <link rel="stylesheet" href="{{asset('web/css/versions.css')}}">--}}
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('web/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('web/css/custom.css')}}">
    <!-- font family -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- end font family -->
    <link rel="stylesheet" href="{{asset('web/css/3dslider.css')}}" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{asset('web/js/3dslider.js')}}"></script>
</head>
<body class="game_info" data-spy="scroll" data-target=".header">
<!-- LOADER -->
<div id="preloader">
    <img class="preloader" src="{{asset('web/images/loading-img.gif')}}" alt="">
</div>
<!-- END LOADER -->
<section id="top" style="padding-bottom: 175px; background-color: yellowgreen;   box-shadow:inset 0 -6em 3em rgb(255 255 255 / 100%), 0 0 0 2px rgb(255 255 255), 0.3em 2.3em 8em rgb(255 255 255 / 10%)">
    @include('partials.head')
</section>
    @yield('content')
    @include('partials.footer')
    @include('partials.modal')
    <a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
    <!-- ALL JS FILES -->
    <script src="{{asset('/web/js/all.js')}}"></script>
    <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="{{asset('/web/js/custom.js')}}"></script>
<script>
    @if(Session::has('message'))
        toastr.options =
        {
            "closeButton" : true,
            "positionClass": "toast-top-full-width",
            "progressBar" : true
        }
    toastr.success("{{ session('message') }}");
    @endif

        @if(Session::has('error'))
        toastr.options =
        {
            "closeButton" : true,
            "positionClass": "toast-top-full-width",
            "progressBar" : true
        }
    toastr.error("{{ session('error') }}");
    @endif

        @if(Session::has('info'))
        toastr.options =
        {
            "closeButton" : true,
            "positionClass": "toast-top-full-width",
            "progressBar" : true
        }
    toastr.info("{{ session('info') }}");
    @endif

        @if(Session::has('warning'))
        toastr.options =
        {
            "closeButton" : true,
            "positionClass": "toast-top-full-width",
            "progressBar" : true
        }
    toastr.warning("{{ session('warning') }}");
    @endif
    $(document).ready( function () {
        var clop = 0;
        var dataSet = [];
        var sda = $('#colum_with_1');
        var t = $('#datatable').DataTable({
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[0]);
            },
            "order": [[ 7, "desc" ]],//desc asc
            language: {
                processing:     "Prebiehajúce ošetrenie ...",
                search:         "Hľadajte v tabuľke ",
                lengthMenu:    "Zobraziť po _MENU_ riadkoch.",
                info:           "Zobrazuje sa _START_ až _END_ zo _TOTAL_ záznamov",
                infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable:     "Aucune donnée disponible dans le tableau",
                paginate: {
                    first:      "Premier",
                    previous:   "Predchádzajúce výsledky",
                    next:       "Ďalšie výsledky",
                    last:       ""
                }
            },
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 8 ] },
                { "bSearchable": false, "aTargets": [ 8 ] }
            ],
            buttons: [
                {
                    text: 'My button',
                    action: function ( e, dt, node, config ) {
                        alert( 'Button activated' );
                    }
                }
            ]
        });
        $("#sortabler").click(function( ){
            var text = $("#colum_with_1").text();
            if(clop < 3){
                $("#colum_with_1").text("1");
                $("#colum_with_0").text("0");
            }
            if(clop > 2){
                $("#colum_with_1").text("5");
                $("#colum_with_0").text("6");
            }
            console.log($("#colum_with_1").text());
            console.log(clop);
            clop = clop +++ 1;
            if(clop === 6){
                clop = 0;
            }
        });
    } );

</script>
</body>
</html>
