@extends('layouts.app')
@section('content')
    <section class="video_section_main theme-padding middle-bg vedio">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table id="datatable" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tím</th>
                            <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Odohraté zápasy">OZ</span></th>{{--Odohraté zápasy--}}
                            <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Výhry">V</span></th>{{--Výhry--}}
                            <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Remízy">R</span></th>{{--Remízy--}}
                            <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Prehry">P</span></th>{{--Prehry--}}
                            <th style="width: 70px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Góly">G</span></th>{{--Góly--}}
                            <th style="width: 40px; text-align: center"><span data-toggle="tooltip" data-placement="top" title="Body">B</span></th>{{--Body--}}
                            <th id="sortabler">Forma</th>{{--Forma--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($matches_data))
                            @foreach($matches_data as $key => $match)
                                <tr>
                                    <td>{{$key + 1 }}</td>
                                    <td>{{$match['team']}}</td>
                                    <td style="text-align: center">{{$match['matches']}}</td>
                                    <td style="text-align: center">{{$match['win']}}</td>
                                    <td style="text-align: center">{{$match['draw']}}</td>
                                    <td style="text-align: center">{{$match['lost']}}</td>
                                    <td style="text-align: center">{{$match['gols']}}</td>
                                    <td style="text-align: center">{{$match['bod']}}</td>
                                    <td style="width: 230px;">
                                        @foreach($match['forma'] as $form)
                                            @if($form['stav'] == 0)<span style="display: none" id="colum_with_0">0</span> @endif
                                            @if($form['stav'] == 1)<span style="display: none" id="colum_with_1">1</span> @endif
                                            @if($form['stav'] == 2)<span style="display: none" id="colum_with_2">2</span> @endif
                                            <div class="form-table-data @if($form['stav'] == 0)form-table-data_lost @else @if($form['stav'] == 1) form-table-data_draw @else  form-table-data_win @endif @endif ">
                                                @if($form['stav'] == 0) P @endif
                                                @if($form['stav'] == 1) R @endif
                                                @if($form['stav'] == 2) V @endif
                                                </div>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
