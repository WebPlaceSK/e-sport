@extends('layouts.app')

@section('content')
    <section id="contant" class="contant main-heading team">
        @foreach($sports as $sport)
            @if($sport->id == $select_sport)
                <h2>LIGY ({{$sport->name}})</h2>
            @endif
        @endforeach
        <div class="row">
            <div class="container">
                @foreach($countrys as $country)

                    <?php $lg = 0; ?>
                    @foreach($leagues as $league)
                        @if($league->country == $country->id)
                            @if($lg == 0)

                                    <h4>{{$country->name}}</h4>
                                <div class="contact">
                            @endif
                            <?php $lg = 1; ?>
                        <div class="col-md-3">
                            <div class="contact-info">
                                <div class="kode-section-title">
                                    <h3>{{$league->name}}</h3>
                                </div>
                                <div class="kode-forminfo">
                                    <ul class="login" style="width: 100%;">
                                        <li class="login-modal">
                                            <div class="cart-option">
                                                <a href="{{route('league.list.detail',['sport' => $select_sport, 'league' => $league->id])}}" class="login" style="cursor: pointer">Otvoriť ligu</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    @if($lg > 0)
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
