@extends('layouts.app')
@section('content')
<section class="video_section_main theme-padding middle-bg vedio">
    <div class="container">
        <div class="row">
            @foreach($sports as $soprt)
            <div class="col-md-3">
                <div class="contact-info">
                    <div class="kode-section-title">
                        <h3>{{$soprt->name}}</h3>
                    </div>
                    <div class="kode-forminfo">
                        <ul class="login" style="width: 100%;">
                            <li class="login-modal">
                                <div class="cart-option">
                                    <a href="{{route('league.list',['sport' => $soprt->id])}}" class="login" style="cursor: pointer">Otvoriť</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach


{{--            <div class="col-md-12">--}}




{{--                <table class="table table-bordered table-hover">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>#</th>--}}
{{--                        <th>Tím</th>--}}
{{--                        <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Odohraté zápasy">OZ</span></th>--}}{{--Odohraté zápasy--}}
{{--                        <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Výhry">V</span></th>--}}{{--Výhry--}}
{{--                        <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Remízy">R</span></th>--}}{{--Remízy--}}
{{--                        <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Prehry">P</span></th>--}}{{--Prehry--}}
{{--                        <th style="width: 70px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Góly">G</span></th>--}}{{--Góly--}}
{{--                        <th style="width: 40px; text-align: center" ><span data-toggle="tooltip" data-placement="top" title="Body">B</span></th>--}}{{--Body--}}
{{--                        <th style="width: 40px; text-align: center" >Forma</th>--}}{{--Forma--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @if(isset($matches))--}}
{{--                    @foreach($matches as $key => $match)--}}
{{--                    <tr>--}}
{{--                        <td>{{$key + 1 }}</td>--}}
{{--                        <td><img src="{{asset('web/images/img-01_004.png')}}" alt="">{{$match->name}}</td>--}}
{{--                        <td style="text-align: center">10</td>--}}
{{--                        <td style="text-align: center">12</td>--}}
{{--                        <td style="text-align: center">20</td>--}}
{{--                        <td style="text-align: center">20</td>--}}
{{--                        <td style="text-align: center">20 : 10</td>--}}
{{--                        <td style="text-align: center">20</td>--}}
{{--                    </tr>--}}
{{--                    @endforeach--}}
{{--                    @else--}}
{{--                        <tr>--}}
{{--                            <td></td>--}}
{{--                            <td></td>--}}
{{--                            <td></td>--}}
{{--                            <td></td>--}}
{{--                            <td></td>--}}
{{--                        </tr>--}}
{{--                    @endif--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}
        </div>
    </div>
</section>
@endsection
