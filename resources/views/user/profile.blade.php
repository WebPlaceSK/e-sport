@extends('layouts.app')

@section('content')
<section id="contant" class="contant main-heading team">

    <h2>Užívateľský profil ({{Auth::user()->name}})</h2>
    <div class="row">
        <div class="container">


            <h4>Nastavenia</h4>
            <div class="contact">

                <div class="col-md-3">
                    <div class="contact-info">
                        <div class="kode-section-title">
                            <h3>fgsdgdf</h3>
                        </div>
                        <div class="kode-forminfo">
                            <ul class="login" style="width: 100%;">
                                <li class="login-modal">
                                    <div class="cart-option">
                                        <a href="fgsdfgds" class="login" style="cursor: pointer">Otvoriť ligu</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2>Obľúbené ligy </h2>
    <div class="row">
        <div class="container">
            <div class="contact">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Liga</th>
                            <th>Krajina</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($matches))
                            @foreach($matches as $key => $match)
                                <tr>
                                    <td>{{$key + 1 }}</td>
                                    <td style="text-align: center">10</td>
                                    <td style="text-align: center">12</td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
