<header>
    <div class="container">
        <div class="header-top">
            <div class="row">
                <div class="col-md-6">
                    <div class="full">
                        <div class="logo">
                            <a href="{{url('/')}}"><img src="{{asset('web/images/logo.png')}}" alt="#" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right_top_section">
                        <!-- social icon -->
                    {{--                            <ul class="social-icons pull-left">--}}
                    {{--                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>--}}
                    {{--                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>--}}
                    {{--                                <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>--}}
                    {{--                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>--}}
                    {{--                            </ul>--}}
                    <!-- end social icon -->
                        <!-- button section -->
                        @if (Route::has('login'))
                            @auth
                                <ul class="login">
                                    @if(Auth::user()->access == 1)
                                    <li class="login-name">
                                        <a href="{{route('admin.index')}}">
                                            Admin
                                        </a>
                                    </li>
                                    @endif
                                    <li class="login-name">
                                        <a href="{{route('user.profile')}}">
                                            {{Auth::user()->name}}
                                        </a>
                                    </li>
                                    <li class="login-modal">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="login"><i class="fa fa-user"></i>Odhlásiť sa</a>
                                    </li>
                                </ul>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            @else
                                <ul class="login">
                                    <li class="login-modal">
                                        <div class="cart-option">
                                            <a class="login" style="cursor: pointer" data-toggle="modal" data-target="#login"><i class="fa fa-user"></i>Prihlásiť</a>
                                        </div>
                                    </li>
                                    @if (Route::has('register'))
                                        <li>
                                            <div class="cart-option">
                                                <a class="login" style="cursor: pointer" data-toggle="modal" data-target="#register">Registrovať</a>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            @endauth
                        @endif
                    <!-- end button section -->
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="row">
                <div class="col-md-12">
                    <div class="full">
                        <div class="main-menu-section">
                            <div class="menu">
                                <nav class="navbar navbar-inverse">
                                    <div class="navbar-header">
                                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="#">Menu</a>
                                    </div>
                                    <div class="collapse navbar-collapse js-navbar-collapse">
                                        <ul class="nav navbar-nav">
                                            <li><a href="{{route('home')}}">Domov</a></li>{{-- class="active-menu"--}}
                                            @foreach($sports as $sport)
                                                <li><a href="{{route('league.list',['sport' => $sport->id])}}">{{$sport->name}}</a></li>
                                            @endforeach
                                            <li><a href="{{route('contact')}}">Kontak</a></li>
                                        </ul>
                                    </div>
                                    <!-- /.nav-collapse -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
