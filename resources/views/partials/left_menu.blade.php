<div class="col-lg-4 col-sm-4 col-xs-12">
    <aside id="sidebar" class="left-bar">
        <div class="banner-sidebar">
            <img class="img-responsive" src="{{asset('web/images/img-05.jpg')}}" alt="#" />
            <h3>Lorem Ipsum is simply dummy text..</h3>
        </div>
    </aside>
    <h4>Match Fixture</h4>
    <aside id="sidebar" class="left-bar">
        <div class="feature-matchs">
            <div class="team-btw-match">
                <ul>
                    <li>
                        <img src="{{asset('web/images/img-01_002.png')}}" alt="">
                        <span>Portugal</span>
                    </li>
                    <li class="vs"><span>vs</span></li>
                    <li>
                        <img src="{{asset('web/images/img-02.png')}}" alt="">
                        <span>Germany</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <img src="{{asset('web/images/img-03_002.png')}}" alt="">
                        <span>Portugal</span>
                    </li>
                    <li class="vs"><span>vs</span></li>
                    <li>
                        <img src="{{asset('web/images/img-04_003.png')}}" alt="">
                        <span>Germany</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <img src="{{asset('web/images/img-05_002.png')}}" alt="">
                        <span>Portugal</span>
                    </li>
                    <li class="vs"><span>vs</span></li>
                    <li>
                        <img src="{{asset('web/images/img-06.png')}}" alt="">
                        <span>Germany</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <img src="{{asset('web/images/img-07_002.png')}}" alt="">
                        <span>Portugal</span>
                    </li>
                    <li class="vs"><span>vs</span></li>
                    <li>
                        <img src="{{asset('web/images/img-08.png')}}" alt="">
                        <span>Germany</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <img src="{{asset('web/images/img-05_002.png')}}" alt="">
                        <span>Portugal</span>
                    </li>
                    <li class="vs"><span>vs</span></li>
                    <li>
                        <img src="{{asset('web/images/img-06.png')}}" alt="">
                        <span>Germany</span>
                    </li>
                </ul>
            </div>
        </div>
    </aside>
    <h4>Points Table</h4>
    <aside id="sidebar" class="left-bar">
        <div class="feature-matchs">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Team</th>
                    <th>P</th>
                    <th>W</th>
                    <th>L</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td><img src="{{asset('web/images/img-01_004.png')}}" alt="">Liverpool</td>
                    <td>10</td>
                    <td>12</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td><img src="{{asset('web/images/img-02_002.png')}}" alt="">Chelsea</td>
                    <td>10</td>
                    <td>12</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td><img src="{{asset('web/images/img-03_003.png')}}" alt="">Norwich City</td>
                    <td>20</td>
                    <td>15</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td><img src="{{asset('web/images/img-04_002.png')}}" alt="">West Brom</td>
                    <td>60</td>
                    <td>10</td>
                    <td>60</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td><img src="{{asset('web/images/img-05.png')}}" alt="">sunderland</td>
                    <td>30</td>
                    <td>06</td>
                    <td>30</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td><img src="{{asset('web/images/img-01_004.png')}}" alt="">Liverpool</td>
                    <td>10</td>
                    <td>12</td>
                    <td>20</td>
                </tr>
                </tbody>
            </table>
        </div>
    </aside>
    <div class="content-widget top-story" style="background: url({{asset('web/images/top-story-bg.jpg')}}">
        <div class="top-stroy-header">
            <h2>Top Soccer Headlines Story <a href="#" class="fa fa-fa fa-angle-right"></a></h2>
            <span class="date">July 05, 2017</span>
            <h2>Other Headlines</h2>
        </div>
        <ul class="other-stroies">
            <li><a href="#">Wenger Vardy won't start</a></li>
            <li><a href="#">Evans: Vardy just</a></li>
            <li><a href="#">Pires and Murray </a></li>
            <li><a href="#">Okazaki backing</a></li>
            <li><a href="#">Wolfsburg's Rodriguez</a></li>
            <li><a href="#">Jamie Vardy compared</a></li>
            <li><a href="#">Arsenal target Mkhitaryan</a></li>
            <li><a href="#">Messi wins libel case.</a></li>
        </ul>
    </div>
</div>
