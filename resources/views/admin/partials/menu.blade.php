<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('admin.index')}}">

        <div class="sidebar-brand-text mx-3">WebPalce <sup>CMS</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @yield('selectSport')">
        <a class="nav-link" href="{{route('admin.select.sport')}}">
            <i class="fas fa-mouse-pointer"></i>
            <span>Vybrať šport</span></a>
    </li>    <!-- Nav Item - Dashboard -->
    @if(Session::has('select_sport'))
        <li class="nav-item @yield('home')">
            <a class="nav-link" href="{{route('admin.index')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Domov (Zápasy)</span></a>
        </li>


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Zápasy
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item @yield('addMatch')">
        <a class="nav-link" href="{{route('admin.add.match')}}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Pridať zápas</span></a>
    </li>

    <div class="sidebar-heading">
        Ostatné
    </div>

    <li class="nav-item @yield('addTeam')">
        <a class="nav-link" href="{{route('admin.add.team')}}">
            <i class="fas fa-users"></i>
            <span>Pridať tím</span></a>
    </li>

    <li class="nav-item @yield('addLeague')">
        <a class="nav-link" href="{{route('admin.add.league')}}">
            <i class="fas fa-baseball-ball"></i>
            <span>Pridať ligu</span></a>
    </li>

@endif

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>


</ul>
