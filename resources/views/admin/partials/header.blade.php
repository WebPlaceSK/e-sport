<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">




    <p style="float: left;font-size: 30px;padding: 15px 0 0 20px;">Vybraný šport: <b>@if(Session::has('select_sport'))
                @foreach($sports as $sport)
                    @if($sport->id == session('select_sport'))
                        {{$sport->name}}
                    @endif
                @endforeach
            @else --- @endif</b></p>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">


        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                {{--                            <span class="badge badge-danger badge-counter">3+</span>--}}
            </a>
            <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    Upozornenie
                </h6>
                {{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
                {{--                                <div class="mr-3">--}}
                {{--                                    <div class="icon-circle bg-warning">--}}
                {{--                                        <i class="fas fa-exclamation-triangle text-white"></i>--}}
                {{--                                    </div>--}}
                {{--                                </div>--}}
                {{--                                <div>--}}
                {{--                                    <div class="small text-gray-500">December 2, 2019</div>--}}
                {{--                                    Spending Alert: We've noticed unusually high spending for your account.--}}
                {{--                                </div>--}}
                {{--                            </a>--}}
                {{--                            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>--}}
            </div>
        </li>

        <!-- Nav Item - Messages -->
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <!-- Counter - Messages -->
                {{--                            <span class="badge badge-danger badge-counter">7</span>--}}
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                    Správy
                </h6>
                {{--                            <a class="dropdown-item d-flex align-items-center" href="#">--}}
                {{--                                <div class="dropdown-list-image mr-3">--}}
                {{--                                    <img class="rounded-circle" src="img/undraw_profile_1.svg"--}}
                {{--                                         alt="">--}}
                {{--                                    <div class="status-indicator bg-success"></div>--}}
                {{--                                </div>--}}
                {{--                                <div class="font-weight-bold">--}}
                {{--                                    <div class="text-truncate">Hi there! I am wondering if you can help me with a--}}
                {{--                                        problem I've been having.</div>--}}
                {{--                                    <div class="small text-gray-500">Emily Fowler · 58m</div>--}}
                {{--                                </div>--}}
                {{--                            </a>--}}
                {{--                            <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>--}}
            </div>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name}}</span>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                 aria-labelledby="userDropdown">
{{--                <a class="dropdown-item" href="#">--}}
{{--                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                    Profile--}}
{{--                </a>--}}
{{--                <a class="dropdown-item" href="#">--}}
{{--                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                    Settings--}}
{{--                </a>--}}
{{--                <a class="dropdown-item" href="#">--}}
{{--                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                    Activity Log--}}
{{--                </a>--}}
{{--                <div class="dropdown-divider"></div>--}}
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Odhlásiť sa
                </a>
            </div>
        </li>

    </ul>

</nav>
