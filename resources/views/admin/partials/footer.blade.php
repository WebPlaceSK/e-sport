<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; <a href="https://www.web-place.sk">WebPlace s.r.o</a> 2021 </span>
        </div>
    </div>
</footer>
