@extends('admin.layouts.app')
@section('home')
    active
@endsection
@section('content')
<h1 class="h3 mb-2 text-gray-800">Zápasy</h1>
<p class="mb-4">Tabuľka a výsledky zápasov.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="homeTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>Team 1</th>
                    <th style="width: 60px">Skóre</th>
                    <th>Team 2</th>
                    <th>Liga</th>
                    <th>Dátum zápasu</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($matchs))
                @foreach($matchs as $match)
                    <tr>
                        @foreach($teams as $team)
                            @if($match->team_1 == $team->id)
                                <td @if($match->score_team_1 > $match->score_team_2) style="background-color: rgb(0 255 0 / 30%);font-weight: 700;" @else @if($match->score_team_1 < $match->score_team_2) style="background-color: rgb(255 0 0 / 30%);font-weight: 700;" @else style="background-color: rgb(255 170 0 / 30%);font-weight: 700;"  @endif @endif >
                                    <span style="display: none">{{$team->name}}</span>
                                    @if($match->team_home == 1)
                                        <i class="fa fa-star" style="color: #ffff00" data-toggle="tooltip" data-placement="bottom" title="Domáci tím" aria-hidden="true"></i>
                                    @endif
                                    {{$team->name}}</td>
                            @endif
                        @endforeach
                        <td style="text-align: center;">{{$match->score_team_1}} : {{$match->score_team_2}}</td>
                        @foreach($teams as $team)
                            @if($match->team_2 == $team->id)
                                    <td @if($match->score_team_1 < $match->score_team_2) style="background-color: rgb(0 255 0 / 30%);font-weight: 700;" @else @if($match->score_team_1 > $match->score_team_2) style="background-color: rgb(255 0 0 / 30%);font-weight: 700;" @else style="background-color: rgb(255 170 0 / 30%);font-weight: 700;"  @endif @endif >
                                    <span style="display: none">{{$team->name}}</span>
                                    @if($match->team_home == 2)
                                        <i class="fa fa-star" style="color: #ffff00" data-toggle="tooltip" data-placement="bottom" title="Domáci tím" aria-hidden="true"></i>
                                    @endif
                                    {{$team->name}}</td>
                            @endif
                        @endforeach
                        @foreach($leagues as $league)
                            @if($match->league == $league->id)
                                <td>{{$league->name}}</td>
                            @endif
                        @endforeach
                        <td>{{date('d.m.Y', strtotime($match->datum))}}</td>
                    </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
        $(document).ready( function () {
            $('#homeTable').DataTable({
                "order": [[ 4, "asc" ]]//desc asc
            });
        } );
    </script>
@endsection
