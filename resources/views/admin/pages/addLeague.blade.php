
@extends('admin.layouts.app')
@section('addLeague')
    active
@endsection
@section('content')
    <h1 class="h3 mb-2 text-gray-800">Pridať ligu</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post" action="{{route('admin.add.league.post')}}">
                @csrf
                <div class="row" style="padding-bottom: 20px">
                    <div class="col">
                        <label for="formGroupTeamName">Názov ligy</label>
                        <input type="text" name="name" class="form-control" id="formGroupTeamName">
                    </div>
                    <div class="col">
                        <label for="formGroupCountry">Krajina ligy</label>
                        <select class="form-control" name="country" id="formGroupCountry">
                            @foreach($countrys as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <button class="btn btn-success btn-lg btn-block">
                            Pridať
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $("#formGroupCountry").select2();
    </script>
@endsection
