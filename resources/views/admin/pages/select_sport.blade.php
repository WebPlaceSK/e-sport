@extends('admin.layouts.app')
@section('selectSport')
    active
@endsection
@section('content')
    <h1 class="h3 mb-2 text-gray-800">Vybrať šport</h1>

    <!-- DataTales Example -->
    <div class="card-body row">
        @foreach($sports as $sport)
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <h4 class="font-weight-bold text-primary text-uppercase mb-1">
                                {{$sport->name}}
                            </h4>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                        </div>
                    </div>
                </div>
                <div style="width: 100%;float: right; padding-left: 10px">
                    <form method="post" action="{{route('admin.select.sport.post')}}">
                        @csrf
                        <input type="hidden" name="id" value="{{$sport->id}}">
                        <button style="float: left;" class="btn btn-success" type="submit">Vybrať tento šport</button>
                    </form>
                    @if($sport->show == 1)
                        <a href="{{route('admin.select.sport.hide', ['id' => $sport->id])}}" style="float: right; padding-right: 10px;">
                            <span aria-hidden="true">Skryť v menu</span>
                        </a>
                    @else
                        <a href="{{route('admin.select.sport.show', ['id' => $sport->id])}}" style="float: right; padding-right: 10px;">
                            <span aria-hidden="true">Ukázať v menu</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
