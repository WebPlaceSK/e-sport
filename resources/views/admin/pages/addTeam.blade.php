@extends('admin.layouts.app')
@section('addTeam')
    active
@endsection
@section('content')
    <h1 class="h3 mb-2 text-gray-800"> Pridať tím</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post" action="{{route('admin.add.team.post')}}">
                @csrf
                <div class="row" style="padding-bottom: 20px">
                    <div class="col">
                        <label for="formGroupTeamName">Názov tímu</label>
                        <input type="text" name="name" class="form-control" id="formGroupTeamName">
                    </div>
                    <div class="col">
                        <label for="formGroupCountry">Krajina tímu</label>
                        <select class="form-control" name="country" id="formGroupCountry">
                            @foreach($countrys as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <label for="formGroupleague">Liga</label>
                        <select class="form-control" name="league" id="formGroupleague">
                            @foreach($leagues as $league)
                                <option value="{{$league->id}}">{{$league->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <button class="btn btn-success btn-lg btn-block">
                            Pridať
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $("#formGroupCountry").select2();
        $("#formGroupleague").select2();
    </script>
@endsection
