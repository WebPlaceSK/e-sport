@extends('admin.layouts.app')
@section('addMatch')
    active
@endsection
@section('content')
    <h1 class="h3 mb-2 text-gray-800"> Pridať zápas</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post" action="{{route('admin.add.match.post')}}">
                @csrf
                <div class="row" style="padding-bottom: 20px">

                    <div class="col-sm-5">
                        <label for="formGroupteam1">Team 1
                            <input class="form-check-input" style="left: 100px;" type="radio" name="home" value="1" id="flexRadioDefault1" checked>
                            <label class="form-check-label" style="padding-left: 30px;" for="flexRadioDefault1">
                                Domáci
                            </label>
                        </label>
                        <select class="form-control" name="team_1" id="formGroupteam1">
                            @foreach($teams as $team)
                                <option value="{{$team->id}}">{{$team->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <label for="formGroupScore1">Skóre Team 1</label>
                        <input type="number" name="score_team_1" class="form-control" id="formGroupScore1" value="0">
                    </div>
                    <div class="col-sm-1">
                        <label for="formGroupScore2">Skóre Team 2</label>
                        <input type="number" name="score_team_2" class="form-control" id="formGroupScore2" value="0">
                    </div>
                    <div class="col-sm-5">
                        <label for="formGroupteam2">Team 2
                            <label class="form-check-label" style="padding-left: 30px;" for="flexRadioDefault1" >
                            <input class="form-check-input" style="left: 100px;" type="radio" name="home" value="2" id="flexRadioDefault1">
                                Domáci
                            </label></label>
                        <select class="form-control" name="team_2" id="formGroupteam2">
                            @foreach($teams as $team)
                                <option value="{{$team->id}}">{{$team->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row" style="padding-bottom: 20px">
                    <div class="col-sm-7">
                        <label for="formGroupleague">Liga</label>
                        <select class="form-control" name="league" id="formGroupleague">
                            @foreach($leagues as $league)
                                <option value="{{$league->id}}">{{$league->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <label for="formGroupScore1">Kurz výhra</label>
                        <input type="text" name="kurz_vyhra" class="form-control" id="formGroupScore1" value="0">
                    </div>
                    <div class="col-sm-1">
                        <label for="formGroupScore2">Kurz remíza</label>
                        <input type="text" name="kurz_remiza" class="form-control" id="formGroupScore2" value="0">
                    </div>
                    <div class="col-sm-1">
                        <label for="formGroupScore2">Kurz prehra</label>
                        <input type="text" name="kurz_prehra" class="form-control" id="formGroupScore2" value="0">
                    </div>
                    <div class="col-sm-2 ">
                        <label for="formGroupScore2">Dátum zápasu</label>
                        <input type="text" name="datum" class="form-control date" value="12-02-2012">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <button class="btn btn-success btn-lg btn-block">
                            Pridať
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $("#formGroupteam1").select2();
        $("#formGroupteam2").select2();
        $("#formGroupleague").select2();
        $.fn.datepicker.defaults.format = "dd.mm.yyyy";
        $('.date').datepicker({
            language: 'sk',
            format: 'dd.mm.yyyy',
        });
    </script>
@endsection
