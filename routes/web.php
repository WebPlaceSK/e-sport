<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', [App\Http\Controllers\LogoutController::class, 'index'])->name('home');
Route::get('/must/login', [App\Http\Controllers\LogoutController::class, 'mustLogin'])->name('must.login');
Route::get('/home', [App\Http\Controllers\LogoutController::class, 'index']);
Route::get('/contact', [App\Http\Controllers\LogoutController::class, 'contact'])->name('contact');
Route::get('/profile', [App\Http\Controllers\UserController::class, 'getProfile'])->name('user.profile');
Route::get('/sport/{sport}/', [App\Http\Controllers\LogoutController::class, 'getLeagueList'])->name('league.list');
Route::get('/sport/{sport}/{league}/', [App\Http\Controllers\LogoutController::class, 'getLeagueListDetail'])->name('league.list.detail');



Route::group(['prefix' => 'user'], function() {
    Route::get('/profile', [App\Http\Controllers\UserController::class, 'getProfile'])->name('user.profile');
});
Route::group(['prefix' => 'adm'], function() {
    //GET
    Route::get('/', [App\Http\Controllers\AdminController::class, 'index'])->name('admin.index');
    Route::get('/select/sport', [App\Http\Controllers\AdminController::class, 'getSelectSport'])->name('admin.select.sport');
    Route::get('/select/sport/show/{id}', [App\Http\Controllers\AdminController::class, 'getSelectSportShow'])->name('admin.select.sport.show');
    Route::get('/select/sport/hide/{id}', [App\Http\Controllers\AdminController::class, 'getSelectSportHide'])->name('admin.select.sport.hide');
    Route::get('/add/match', [App\Http\Controllers\AdminController::class, 'getAddMatch'])->name('admin.add.match');
    Route::get('/add/team', [App\Http\Controllers\AdminController::class, 'getAddTeam'])->name('admin.add.team');
    Route::get('/add/league', [App\Http\Controllers\AdminController::class, 'getAddLeague'])->name('admin.add.league');

    //POST
    Route::post('/select/sport', [App\Http\Controllers\AdminController::class, 'postSelectSport'])->name('admin.select.sport.post');
    Route::post('/add/match', [App\Http\Controllers\AdminController::class, 'postAddMatch'])->name('admin.add.match.post');
    Route::post('/add/team', [App\Http\Controllers\AdminController::class, 'postAddTeam'])->name('admin.add.team.post');
    Route::post('/add/league', [App\Http\Controllers\AdminController::class, 'postAddLeague'])->name('admin.add.league.post');

});

